# Dump disk
Classic dd de l'image raspbian

# Preconfiguration avant boot

```shell
#/boot/cmdline.txt
dwc_otg.lpm_enable=0 console=serial0,115200 console=tty1 root=PARTUUID=c5d0b17c-02 rootfstype=ext4 elevator=deadline fsck.repair=yes rootwait modules-load=dwc2,g_multi
#/boot/config.txt (ajout a la fin)
enable_uart=1
dtoverlay=dwc2,dr_mode=peripheral
#ajouter un fichier ssh dans /boot pour activer ssh
touch /boot/ssh
#ajouter dans /etc/modules :
dwc2
g_multi
```

# Config après reboot
  
En gros il faut l'ensemble des services avahi(chez moi ça n'a pas suffit mais ma conf est louche donc je ne vais pas chercher pourquoi)

```shell
sudo raspi-config
# cofigurer le wifi
```

! test d'utilisation comme clé usb

source : https://www.raspberrypi.org/magpi/pi-zero-w-smart-usb-flash-drive/

# Conf static de l'usb0

```shell
# ajout dans /etc/rc.local
modprobe -r g_multi
modprobe g_multi file=/piusb.img ro=0 host_addr=11:22:33:44:55:66 dev_addr=aa:bb:cc:dd:ee:ff
su - pi -c "/usr/bin/syncthing"&
```

# Installation syncthing

```shell
sudo apt-get install syncthing
```

# faire du g_ether et du g_masstorage en meme temps

https://www.isticktoit.net/?p=1383