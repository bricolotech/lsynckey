# Configuration réseau

root@OpenWrt:/# cat /etc/config/wireless

```shell
config wifi-device 'radio0'
        option type 'mac80211'
        option channel '11'
        option hwmode '11g'
        option path 'platform/soc/20300000.mmc/mmc_host/mmc1/mmc1:0001/mmc1:0001:1'
        option htmode 'HT20'
        option disabled '0'

config 'wifi-iface'
        option device 'radio0'
        option network    'lan'
        option mode       'sta'
        option ssid       'Livebox-CFFC'
        option encryption 'psk2'
        option key        'CF5341C5D91651E54127C6E947'
```

root@OpenWrt:/# cat /etc/config/network
```shell
config interface 'loopback'
        option ifname 'lo'
        option proto 'static'
        option ipaddr '127.0.0.1'
        option netmask '255.0.0.0'

config globals 'globals'
        option ula_prefix 'fd30:536e:c089::/48'

config interface 'lan'
        option ifname 'wlan0'
        option proto 'dhcp'
        option ip6assign '60'
```
Then validate

```shell
uci set wireless.@wifi-device[0].disabled=0
uci commit wireless
wifi
```

# Installation syncthing

```shell
# creer une nouvelle partition

opkg update 
opkg install fdisk
fdisk /dev/mmcblk0

# creer une partition avec le reste puis sauvegarde + reboot
# ensuite on fait comme pour le mybook live

opkg update
opkg install kmod-fs-ext4 e2fsprogs block-mount
time mkfs.ext4 -L Data /dev/mmcblk0p3
mkdir -p /mnt/Data

# un petit test
time mount -v -o sync /dev/mmcblk0p3 /mnt/Data

# Unmount ext4 data partition
time umount -v /mnt/Data/

#ajout de la data

uci set fstab.@global[0].check_fs='1' # This will run a fsck (like chkdsk) at startup

uci add fstab mount
uci set fstab.@mount[-1].target='/mnt/Data'
uci set fstab.@mount[-1].device='/dev/mmcblk0p3'
uci set fstab.@mount[-1].enabled='1'
uci set fstab.@mount[-1].enabled_fsck='1'
uci set fstab.@mount[-1].options='rw,sync,noatime,nodiratime'

# histoire de voir si tt fonctionne
reboot

chmod a+w /mnt/Data
chmod a+w /mnt/Data -R

# ajout d'un utilisateur
opkg update && opkg install shadow-useradd
opkg install procps-ng-pkill
opkg install curl
curl -L https://github.com/syncthing/syncthing/releases/download/v1.1.3/syncthing-linux-arm-v1.1.3.tar.gz -o syncthing.tgz
tar -xzf syncthing.tgz
cd syncthing*
mv syncthing /usr/bin
```

# Configuration syncthing

```shell
# startup script - c'est moche mais c'est mieux que de lancer en root
root@OpenWrt:~/syncthing-linux-arm-v1.1.3# cat /etc/init.d/syncthing
#!/bin/sh /etc/rc.common
# Copyright (C) 2015 brglng@github.com

START=99
STOP=99

start() {
        service_start /usr/bin/su - thierry -c "/usr/bin/syncthing 2>&1 | logger -t syncthing" &
}

stop() {
        /usr/bin/pkill syncthing
}

/etc/init.d/syncthing enable

# configuration firewall

# source : https://github.com/brglng/syncthing-openwrt

# dans le fichier /etc/config/firewall

config rule
         option enabled '1'
         option target 'ACCEPT'
         option src 'lan'
         option proto 'tcp'
         option dest_port '8384'
         option name 'Syncthing Web'
config rule
         option target 'ACCEPT'
         option src 'wan'
         option proto 'tcp'
         option dest_port '22000'
         option name 'Syncthing TCP'

config rule
         option enabled '1'
         option target 'ACCEPT'
         option src 'wan'
         option proto 'udp'
         option dest_port '21025'
         option name 'Syncthing UDP'

config redirect
         option enabled '1'
         option target 'DNAT'
         option src 'wan'
         option dest 'lan'
         option proto 'tcp'
         option src_dport '22000'
         option dest_ip '192.168.1.1'
         option dest_port '22000'
         option name 'Syncthing'

/etc/init.d/firewall restart

#S'il n'y a pas d'erreur allez dans l'interface d'admin et choisissez vite un user/passwd #TODO : le mettre dans le fichier de conf ~/.config/sync

