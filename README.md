# LSyncKey

Contraction de Libre Sync Key. C'est une clé de synchronisation de données basé sur une chaine logicielle entièrement libre. A terme nous aimerions que le matériel le soit aussi mais par facilité dans un premier temps nous avons choisi le raspi zero.

[Installation sur raspbian](installation_raspbian.md)  
[Installation sur openwrt](installation_openwrt.md)
