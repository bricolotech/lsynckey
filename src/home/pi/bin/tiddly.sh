#!/usr/bin/env bash
export NODEJSHOME=$HOME/Outils/node-v10.15.3-linux-armv6l
export TIDDLYWIKIHOME=$HOME/partages/tiddlywiki
export PATH=$PATH:$NODEJSHOME/bin
export log_file=$HOME/go.log
export firefox_path=$HOME/bin/firefox
export wiki_url=http://169.254.42.77:8080
function log_wiki {
  message=$1
  date "+%y%m%d %H:%M:%S $message" >> $log_file
}
function check_wiki {
  log_wiki ${FUNCNAME[0]}
  curl $wiki_url
}
function start_wiki {
  log_wiki ${FUNCNAME[0]}
  (cd $TIDDLYWIKIHOME && tiddlywiki monWiki --server 8080 "" "" "" "" "" 169.254.42.77&)
}
function show_wiki {
  log_wiki ${FUNCNAME[0]}
 sleep 1 && $firefox_path --new-tab $wiki_url
}
function kill_wiki {
  log_wiki ${FUNCNAME[0]}
  kill $(ps -ef | grep "monWiki --[s]erver" | awk '{print $2}')
}

if [ "$1" == "stop" ];then
  kill_wiki
  exit 0
fi
check_wiki
if [ $? -gt 0 ];then
  start_wiki 2>&1 >>$HOME/go.log && show_wiki >>$HOME/go.log 2>&1
  exit 0
else
  show_wiki
  exit 0
fi
exit 1
